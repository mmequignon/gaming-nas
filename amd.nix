{ config, pkgs, ... }:

{

  hardware.enableRedistributableFirmware = true;
  services.xserver.videoDrivers = [ "amdgpu" ];
  environment = {
    systemPackages = with pkgs; [
      radeontop radeon-profile
    ];
  };

}
