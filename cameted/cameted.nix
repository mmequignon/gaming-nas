{ config, pkgs, ... }:

let
  MPD_MUSIC_DIRECTORY = "/srv/share/music";
in {
  imports = [
    <home-manager/nixos>
  ];

  users.extraUsers.cameted = {
    shell = pkgs.zsh;
    extraGroups = [
      "audio"
    ];
    isNormalUser = true;
  };
  home-manager.users.cameted = {
    services.mpd = {
      enable = true;
      musicDirectory = "${MPD_MUSIC_DIRECTORY}";
      network = {
        listenAddress = "0.0.0.0";
        port = 6666;
        startWhenNeeded = true;
      };
      extraConfig = ''
        audio_output {
          type            "httpd"
          name            "Streaming ogg vorbis"
          encoder         "lame"
          port            "6667"
          quality         "5.0"
          format          "44100:16:2"
        }
      '';
    };
  };
  networking.firewall = {
    allowedTCPPorts = [ 6600 ];
  };
}
