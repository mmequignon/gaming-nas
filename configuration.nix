# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./matthieu/matthieu.nix
      ./cameted/cameted.nix
      ./steam/steam.nix
      ./amd.nix
      ./powersave.nix
      ./services.nix
    ];

  nix.readOnlyStore = false; # To make the store compressible via btrfs
  nixpkgs.config.allowBroken = true;

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;
    # tmpOnTmpfs = true;
    extraModulePackages = with config.boot; [ kernelPackages.r8125 ];
  };

  environment = {
    systemPackages = with pkgs; [
      # utils
      vimHugeX htop sudo pciutils lshw python3Full fuse sshfs-fuse
      docker-compose acpi usbutils cifs-utils
      # Btrfs : systemwide
      compsize
      # get mapping /dev/sdX -> sataY : lsscsi --verbose
      lsscsi
    ];
  };



  time.timeZone = "Europe/Paris";

  networking = {
    networkmanager.enable = true;
    usePredictableInterfaceNames = false;
    firewall.allowedTCPPorts = [61208];
    hostName = "nixgn";
  };

  # SOUND
  sound.enable = true;
  hardware = {
    pulseaudio = {
      enable = true;
      support32Bit = true;
      package = pkgs.pulseaudioFull.override {
        alsa-lib = pkgs.alsa-lib.overrideAttrs (drv: rec {
          version = assert pkgs.alsa-lib.version == "1.2.5"; "1.2.5.1";
          src = pkgs.fetchurl {
            url = "mirror://alsa/lib/${drv.pname}-${version}.tar.bz2";
            hash = "sha256-YoQh2VDOyvI03j+JnVIMCmkjMTyWStdR/6wIHfMxQ44=";
          };
        });
      };
    };
    logitech.wireless.enable = true;
    logitech.wireless.enableGraphical = true;
  };

  # KEYBOARD / FONT
  console.font = "Lat2-Terminus16";
  i18n = {
    defaultLocale = "fr_FR.UTF-8";
  };

  fonts = {
    fontconfig.dpi = 96;
    fontDir.enable = true;
    fonts = with pkgs; [
      jetbrains-mono
      fira-code
      iosevka
    ];
  };

  system.stateVersion = "20.03"; # Did you read the comment?
}
