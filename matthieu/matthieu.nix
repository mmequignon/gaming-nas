{ config, pkgs, ... }:

let
  fwzte_matthieu_address = import ./secrets/malignly.nix;
  gpg_signing_key = import ./secrets/indented.nix;
  authorized_keys = import ./secrets/manageress.nix;
  MPD_MUSIC_DIRECTORY = "/srv/share/music";
in {
  imports = [
    <home-manager/nixos>
    ./packages.nix
    ./zsh.nix
    ./gpg.nix
  ];

  nixpkgs.config.allowUnfree = true;

  users.extraUsers.matthieu = {
    shell = pkgs.zsh;
    extraGroups = [
      "wheel"
      "docker"
      "networkmanager"
      "audio"
      "fuse"
      "video"
    ];
    isNormalUser = true;
    openssh.authorizedKeys.keys = authorized_keys;
  };



  home-manager.users.matthieu = {

    services.caffeine.enable = true;
    services.mpd = {
      enable = true;
      musicDirectory = "${MPD_MUSIC_DIRECTORY}";
      network = {
        listenAddress = "0.0.0.0";
        port = 6600;
        startWhenNeeded = true;
      };
      extraConfig = ''
        audio_output {
          type "pulse"
          name "output:iec958"
        }
      '';
    };
    programs.git = {
      package = pkgs.gitAndTools.gitFull;
      enable = true;
      userEmail = "${fwzte_matthieu_address}";
      signing = {
        key = "${gpg_signing_key}";
        signByDefault = true;
      };
      aliases = {
        glog = "log --graph --decorate";
        ci = "commit";
        st = "status";
      };
      extraConfig = {
        core = {
          editor = "vim";
          pager = "";
          excludesfile = "~/.gitignore";
        };
        pull = {
          rebase = true;
        };
        user = {
          name = "Mmequignon";
        };
        hub = {
          protocol = "ssh";
        };
        init = {
          defaultBranch = "master";
        };
      };
    };

    programs.vim = {
      enable = true;
      extraConfig = builtins.readFile dotfiles/vimrc;
      plugins = with pkgs.vimPlugins; [
        ale
        nerdtree
        undotree
        vim-signify
        vim-airline
        vim-airline-themes
        vim-devicons
        fzf-vim
        ack-vim
        tagbar
        vim-colorschemes
        vim-nix
        vim-easymotion
        limelight-vim
        vim-fugitive
        fugitive-gitlab-vim
        nerdcommenter
        rust-vim
        ultisnips
      ];
    };

    #programs.neovim = {
      #enable = true;
      #extraConfig = builtins.readFile dotfiles/vimrc;
      #extraPython3Packages = (ps: with ps; [
        #pylint
      #]);
      #plugins = with pkgs.vimPlugins; [
        #ale
        #nerdtree
        #undotree
        #vim-signify
        #vim-airline
        #vim-airline-themes
        #vim-devicons
        #fzf-vim
        #ack-vim
        #tagbar
        #vim-colorschemes
        #vim-nix
        #vim-easymotion
        #nerdcommenter
      #];
    #};

    programs.tmux = {
      enable = true;
      extraConfig = builtins.readFile dotfiles/tmux.conf;
      plugins = with pkgs; [
        {
          plugin = tmuxPlugins.sidebar;
          extraConfig = "set -g @sidebar-tree-command 'tree -C'";
        }
      ];
    };

    xdg.configFile = {
      "terminator/config".source = ./dotfiles/terminator.conf;
      "mc".source = ./dotfiles/mc;
    };

    home.file = {
      ".vim/pylintrc".source = ./dotfiles/pylintrc;
      ".vim/colors/selenized.vim".source = ./dotfiles/selenized.vim;
      ".config/nvim/colors/selenized.vim".source = ./dotfiles/selenized.vim;
    };

    manual.manpages.enable = true;
  };
  networking.firewall = {
    allowedTCPPorts = [ 6600 ];
  };
}
