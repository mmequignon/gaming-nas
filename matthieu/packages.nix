{ config, pkgs, ...}:

{
  users.users.matthieu.packages = with pkgs; [
    tree ack silver-searcher gnumake mc ranger unzip lm_sensors
    sysfsutils mkpasswd fzf p7zip unrar jmtpfs alsaUtils
    pinentry-curses terminator
    ## NET TOOLS
    nethogs tcpdump wget curl
    ## Nix tools
    nix-prefetch-git nix-prefetch-github
    ## Code
    # Git
    git-crypt gitAndTools.hub gitAndTools.lab
    # Python
    python3Full
    # Rust
    # Editor & tools
    python3Packages.pylint nixfmt ctags 
  ];
}
