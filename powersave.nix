{ config, pkgs, ... }:

{

  powerManagement = {
    enable = true;
    cpuFreqGovernor = "ondemand";
  };

}
