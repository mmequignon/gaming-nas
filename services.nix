{ config, pkgs, ... }:

{
  imports = [
    ./services/services.nix
  ];

  environment.systemPackages = with pkgs; [
    glances
    i3lock
  ];

  systemd.services.glances = {
    description = "glances";
    serviceConfig.ExecStart = ''${pkgs.glances}/bin/glances -w'';
    wantedBy = ["multi-user.target"];
  };

  services = {
    upower.enable = true;

    dbus = {
      enable = true;
      packages = with pkgs; [
        gnome3.dconf
      ];
    };

    openssh = {
      enable = true;
      forwardX11 = true;
      passwordAuthentication = false;
    };

    xserver = {
      enable = true;
      windowManager.awesome = {
        enable = true;
        luaModules = with pkgs; [
          lua52Packages.vicious
        ];
      };
      displayManager = {
        autoLogin = {
          enable = true;
          user = "steam";
        };
        lightdm = {
          enable = true;
        };
      };
      # keyboard
      layout = "fr,us";
      xkbOptions = "compose:menu";
    };
  };
}
