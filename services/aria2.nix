{config, lib, pkgs, ...}:

with lib;

let
  RPC_SECRET = import ./secrets/graftage.nix;
in {
  services.aria2 = {
    enable = true;
    rpcSecret = "${RPC_SECRET}";
    extraArguments = "--continue=true --enable-rpc=true --rpc-listen-all=true --console-log-level=warn --log-level=notice --file-allocation=trunc --max-connection-per-server=4 --max-concurrent-downloads=4 --max-overall-download-limit=0 --bt-force-encryption=true --check-integrity=true --enable-dht=true --enable-peer-exchange=true --follow-torrent=true --seed-time=0 --min-split-size=25M --check-certificate=false --save-session-interval=2 --rpc-save-upload-metadata=true --force-save=true --enable-dht6=false --disable-ipv6=true --log=/var/lib/aria2/aria2.log";
  };

  users.users.aria2.extraGroups = ["nextcloud"];
}
