{ config, pkgs, ... }:

let
  BORG_PATH = "/srv/share/borg/";
  MATTHIEU_SSH_PUB_KEY = import ./secrets/deflected.nix;
in {
  services.borgbackup.repos.matthieu_home = {
    path = "${BORG_PATH}matthieu_home";
    authorizedKeys = [
      "${MATTHIEU_SSH_PUB_KEY}"
    ];
  };
  environment = {
    systemPackages = with pkgs; [
      borgbackup
    ];
  };

}
