{config, pkgs, ...}:

let
  GRAFANA_DBNAME = "grafana";
  GRAFANA_DBUSER = "grafana";
  GRAFANA_DBPASS = "_8;0N<C24<0KU+oV3n~[;6TOP";
in {
  services.grafana = {
    enable = true;
    # smtp = {
    #   user = "monitoring@fzwte.xyz";
    # };
    addr = "0.0.0.0";
    port = 3000;
    database = {
      type = "postgres";
      name = "${GRAFANA_DBNAME}";
      host = "localhost";
      password = "${GRAFANA_DBPASS}";
      user = "${GRAFANA_DBUSER}";
    };
    # provision.datasources = {
    #   influxdb = {
    #   };
    # };
  };

  services.postgresql = {
    ensureDatabases = [ "${GRAFANA_DBNAME}" ];
    ensureUsers = [
      {
        name = "${GRAFANA_DBUSER}";
        ensurePermissions."DATABASE ${GRAFANA_DBNAME}" = "ALL PRIVILEGES";
      }
    ];
  };

  networking.firewall = {
    allowedTCPPorts = [ 3000 ];
  };
}
