{config, pkgs, ...}:

{
  services.influxdb = {
    enable = true;
    dataDir = "/srv/share/influxdb";
  };
}
