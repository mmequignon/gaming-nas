{config, pkgs, ...}:

{

  services.mopidy = {
    enable = true;
    configuration = builtins.readFile configs/mopidy.conf;
    extensionPackages = with pkgs; [
      mopidy-mpd
    ];
  };

  networking.firewall = {
    allowedTCPPorts = [ 6600 ];
  };

  users.extraUsers.mopidy = {
    extraGroups = [
      "audio"
    ];
  };

}
