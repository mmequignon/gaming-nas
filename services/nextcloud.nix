{config, pkgs, ...}:
let 
  NEXTCLOUD_HOST = import secrets/revilingly.nix;
  NEXTCLOUD_DBNAME = "nextcloud";
  NEXTCLOUD_DBUSER = "nextcloud";
  NEXTCLOUD_DBPASS = import secrets/ordinator.nix;
  NEXTCLOUD_ADMINPASS = import secrets/examination.nix;
  NEXTCLOUD_HOME = "/srv/share/nextcloud";
in {

  services.nginx.virtualHosts = {
    "${NEXTCLOUD_HOST}" = {
       ## Force HTTP redirect to HTTPS
       forceSSL = true;
       ## LetsEncrypt
       enableACME = true;
    };
  };

  services.postgresql = {
    ensureDatabases = [ "${NEXTCLOUD_DBNAME}" ];
    ensureUsers = [
      {
        name = "${NEXTCLOUD_DBUSER}";
        ensurePermissions."DATABASE ${NEXTCLOUD_DBNAME}" = "ALL PRIVILEGES";
      }
    ];
  };

  services.nextcloud = {
    enable = true;
    package = pkgs.nextcloud21;
    home = "${NEXTCLOUD_HOME}";
    hostName = "${NEXTCLOUD_HOST}";
    phpExtraExtensions = all: [all.curl];
    autoUpdateApps = {
      enable = true;
      startAt = "05:00:00";
    };
    config = {
      overwriteProtocol = "https";
      dbtype = "pgsql";
      dbuser = "nextcloud";
      dbhost = "/run/postgresql"; # nextcloud will add /.s.PGSQL.5432 by itself
      dbname = "nextcloud";
      dbpass = "${NEXTCLOUD_DBPASS}";
      adminpass = "${NEXTCLOUD_ADMINPASS}";
      adminuser = "admin";
      defaultPhoneRegion = "FR";
    };
  };
  systemd.services."nextcloud-setup" = {
    requires = ["postgresql.service"];
    after = ["postgresql.service"];
  };
  environment = {
    systemPackages = with pkgs; [
      ffmpeg youtube-dl
    ];
  };
}
