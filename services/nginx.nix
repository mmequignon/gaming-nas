{config, pkgs, ...}:

let
  EMAIL = import ./secrets/kleinite.nix;
in {
  security.acme = {
    acceptTerms = true;
    email = "${EMAIL}";
  };
  services.nginx = {
     enable = true;
     recommendedGzipSettings = true;
     recommendedOptimisation = true;
     recommendedProxySettings = true;
     recommendedTlsSettings = true;
     # Only allow PFS-enabled ciphers with AES256
     # sslCiphers = "AES256+EECDH:AES256+EDH:!aNULL";
  };

  networking.firewall = {
    allowedTCPPorts = [ 80 443 ];
  };
  users.users.nginx.extraGroups = [ "aria2" ];
}
