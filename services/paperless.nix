{ config, pkgs, ... }:

let
  PAPERLESS-DATA = "/srv/share/paperless/data";
  PAPERLESS-CONSUMPTION = "/srv/share/paperless/consumption";
in {
  services = {
     paperless = {
       enable = true;
       address = "0.0.0.0";
       port = 8080;
       consumptionDir = "${PAPERLESS-CONSUMPTION}";
       consumptionDirIsPublic = true;
       dataDir = "${PAPERLESS-DATA}";
       extraConfig = {
         PAPERLESS_DISABLE_LOGIN = "true";
       };
     };
     samba.shares.paperless = {
       path = "${PAPERLESS-CONSUMPTION}";
       browseable = "yes";
       "read only" = "no";
       "guest ok" = "no";
       "create mask" = "0644";
       "directory mask" = "0755";
       "valid users" = "paperless";
     };
  };
  users.extraUsers.paperless = {
    isNormalUser = false;
  };
  networking.firewall = {
    allowedTCPPorts = [ 8080 ];
  };

}
