{config, pkgs, ...}:

{
  services.postgresql = {
    enable = true;
    dataDir = "/srv/share/postgresql/${config.services.postgresql.package.psqlSchema}";
  };
}
