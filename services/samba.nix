{ config, pkgs, ... }:

let
  VIDEOS_PATH = "/srv/share/videos";
  MUSIC_PATH = "/srv/share/music";
in {
  services = {
    samba = {
      enable = true;
      securityType = "user";
      extraConfig = ''
        workgroup = gamingnas
        server string = smbnix
        netbios name = smbnix
        security = user
        hosts allow = 0.0.0.0/0
        guest account = nobody
        map to guest = bad user
        load printers = no
        printcap name = /dev/null
      '';
      shares = {
        music = {
          path = "${MUSIC_PATH}";
          browseable = "yes";
          "read only" = false;
          "guest ok" = "yes";
          "create mask" = "0644";
          "directory mask" = "0755";
        };
        videos = {
          path = "${VIDEOS_PATH}";
          browseable = "yes";
          "read only" = false;
          "guest ok" = "yes";
          "create mask" = "0644";
          "directory mask" = "0755";
        };
      };
    };
  };
  networking.firewall = {
    # SAMBA
    allowedTCPPorts = [ 445 139 ];
    allowedUDPPorts = [ 137 138 ];
  };
}
