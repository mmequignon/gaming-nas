{ config, pkgs, ... }:

{
  imports = [
    ./borg.nix
    ./nextcloud.nix
    ./nginx.nix
    ./postgres.nix
    ./samba.nix
    # ./torrent.nix
    # ./paperless.nix
    # ./services/aria2.nix
    # ./services/mopidy.nix
    # ./services/monitoring.nix
  ];
}
