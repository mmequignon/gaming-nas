{config, pkgs, ...}:

{

  services = {
    deluge = {
      enable = true;
      dataDir = "/srv/share/deluge";
      web = {
        enable = true;
        port = 5678;
        openFirewall = true;
      };
    };
  };

  users.extraUsers.deluge.isNormalUser = true;

}
