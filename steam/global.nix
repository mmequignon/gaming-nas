{ config, pkgs, ... }:

{

  # 32 bits support for steam
  hardware.opengl.driSupport32Bit = true;
  hardware.opengl.extraPackages32 = with pkgs.pkgsi686Linux; [ libva zlib ];
  hardware.pulseaudio.support32Bit = true;

}
