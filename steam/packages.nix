{ config, pkgs, ...}:

{
  users.users.steam.packages = with pkgs; [
    networkmanagerapplet pavucontrol unzip pwgen lm_sensors kitty
    mkpasswd p7zip unrar flameshot peek
    # (steam.override {
    #   extraPkgs = pkgs: [
    #     libva
    #     zlib
    #     xorg.xkbutils
    #     xorg.libX11
    #     xorg.libXext
    #     xorg.libXcursor
    #     xorg.libXrandr
    #   ];
    # })
    # steam-run-native
    steam
    xboxdrv
    wineWowPackages.stable
    protontricks
    winetricks
  ];
}
