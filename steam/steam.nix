{ config, pkgs, ... }:

let
  authorized_keys = import ./secrets/manageress.nix;
in {

  imports = [
    <home-manager/nixos>
    ./global.nix
    ./packages.nix
    ./zsh.nix
  ];

  users.extraUsers.steam = {
    shell = pkgs.zsh;
    extraGroups = [
      "wheel"
      "networkmanager"
      "audio"
      "fuse"
      "video"
    ];
    isNormalUser = true;
    openssh.authorizedKeys.keys = authorized_keys;
  };

  # Configuration awesome
  home-manager.users.steam = {
    gtk = {
      enable = true;
      theme = {
        name = "Adwaita-dark";
        package = pkgs.gnome3.gnome_themes_standard;
      };
    };

    xdg.configFile = {
      "awesome".source = ./dotfiles/awesome;
    };

  };
}
