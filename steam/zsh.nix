{ config, pkgs, ...}:

{
  environment.shells = [
    pkgs.bashInteractive pkgs.zsh
  ];

  home-manager.users.steam = {

    programs.zsh = {
      enable = true;
      enableAutosuggestions = true;
      enableCompletion = true;
      localVariables = {
        POWERLEVEL9K_MODE = "nerdfont-complete";
        POWERLEVEL9K_LEFT_PROMPT_ELEMENTS = ["os_icon" "context" "virtualenv" "vcs" "dir"];
        POWERLEVEL9K_LEFT_SEGMENT_SEPARATOR = "";
        POWERLEVEL9K_LEFT_SUBSEGMENT_SEPARATOR = "";
        POWERLEVEL9K_WHITESPACE_BETWEEN_LEFT_SEGMENTS = " ";
        POWERLEVEL9K_DISABLE_RPROMPT = true;
        POWERLEVEL9K_LOAD_WHICH = 1;
        POWERLEVEL9K_SHORTEN_STRATEGY = "truncate_to_first_and_last";
        POWERLEVEL9K_SHORTEN_DIR_LENGTH = 1;
        POWERLEVEL9K_RAM_BACKGROUND = "none";
        POWERLEVEL9K_RAM_FOREGROUND = 133;
        POWERLEVEL9K_VIRTUALENV_BACKGROUND = "none";
        POWERLEVEL9K_VIRTUALENV_FOREGROUND = 220;
        POWERLEVEL9K_LOAD_CRITICAL_BACKGROUND = "none";
        POWERLEVEL9K_LOAD_CRITICAL_FOREGROUND = 001;
        POWERLEVEL9K_LOAD_WARNING_BACKGROUND = "none";
        POWERLEVEL9K_LOAD_WARNING_FOREGROUND = 172;
        POWERLEVEL9K_LOAD_NORMAL_BACKGROUND = "none";
        POWERLEVEL9K_LOAD_NORMAL_FOREGROUND = 040;
        POWERLEVEL9K_BATTERY_LOW_BACKGROUND = "none";
        POWERLEVEL9K_BATTERY_LOW_FOREGROUND = 001;
        POWERLEVEL9K_BATTERY_CHARGING_BACKGROUND = "none";
        POWERLEVEL9K_BATTERY_CHARGING_FOREGROUND = 172;
        POWERLEVEL9K_BATTERY_CHARGED_BACKGROUND = "none";
        POWERLEVEL9K_BATTERY_CHARGED_FOREGROUND = 040;
        POWERLEVEL9K_BATTERY_DISCONNECTED_BACKGROUND = "none";
        POWERLEVEL9K_BATTERY_DISCONNECTED_FOREGROUND = 001;
        POWERLEVEL9K_OS_ICON_BACKGROUND = "none";
        POWERLEVEL9K_OS_ICON_FOREGROUND = "124";
        POWERLEVEL9K_VCS_CLEAN_BACKGROUND = "none";
        POWERLEVEL9K_VCS_CLEAN_FOREGROUND = "076";
        POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND = "none";
        POWERLEVEL9K_VCS_UNTRACKED_FOREGROUND = "005";
        POWERLEVEL9K_VCS_MODIFIED_BACKGROUND = "none";
        POWERLEVEL9K_VCS_MODIFIED_FOREGROUND = "003";
        POWERLEVEL9K_HOME_ICON = "";
        POWERLEVEL9K_HOME_SUB_ICON = "";
        POWERLEVEL9K_FOLDER_ICON = "";
        POWERLEVEL9K_ETC_ICON = "";
        POWERLEVEL9K_CONTEXT_DEFAULT_BACKGROUND = "none";
        POWERLEVEL9K_CONTEXT_ROOT_BACKGROUND = "none";
        POWERLEVEL9K_CONTEXT_SUDO_BACKGROUND = "none";
        POWERLEVEL9K_CONTEXT_REMOTE_BACKGROUND = "none";
        POWERLEVEL9K_CONTEXT_REMOTE_SUDO_BACKGROUND = "none";
        POWERLEVEL9K_DIR_ETC_BACKGROUND = "none";
        POWERLEVEL9K_DIR_ETC_FOREGROUND = "203";
        POWERLEVEL9K_DIR_HOME_BACKGROUND = "none";
        POWERLEVEL9K_DIR_HOME_FOREGROUND = "045";
        POWERLEVEL9K_DIR_DEFAULT_BACKGROUND = "none";
        POWERLEVEL9K_DIR_DEFAULT_FOREGROUND = "045";
        POWERLEVEL9K_DIR_HOME_SUBFOLDER_BACKGROUND = "none";
        POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND = "045";
        POWERLEVEL9K_STATUS_ERROR_BACKGROUND = "none";
        POWERLEVEL9K_STATUS_OK_BACKGROUND = "none";
      };
      plugins = [
          {
            name = "powerlevel9k";
            file = "powerlevel9k.zsh-theme";
            src = pkgs.fetchFromGitHub {
              owner = "Powerlevel9k";
              repo = "powerlevel9k";
              rev = "3dafd79c41f8601b055e607ffefbfe3250c26040";
              sha256 = "0vc5d7w8djg3ah9jvd87xqbhpin1lpflm6wgmhn3jgijwcjkxpg3";
            };
          }
      ];
      shellAliases = {
        cpr = "rsync -a --info = progress2";
        passgen = "pwgen -sy 15 1";
        ls = "ls -sh --color=auto ";
        ll = "ls -lh";
        lla = "ls -lah";
        llas = "ls -laSh";
        grep = "grep --color";
        tmux = "tmux -2";
        glances = "nix-shell -p python37Packages.glances --run 'glances'";
        next = "WEBKIT_DISABLE_COMPOSITING_MODE=1 next";
      };
      sessionVariables = {
        EDITOR = "vim";
        TERM = "xterm-256color";
      };
      initExtra = ''
        bindkey "\eOH" beginning-of-line
        bindkey "\eOF" end-of-line
        bindkey "\e[5~" beginning-of-history
        bindkey "\e[6~" end-of-history
        bindkey "\e[3~" delete-char
        bindkey "\e[2~" quoted-insert
        bindkey "\e[5C" forward-word
        bindkey "\eOc" emacs-forward-word
        bindkey "\e[5D" backward-word
        bindkey "\eOd" emacs-backward-word
        bindkey "\e\e[C" forward-word
        bindkey "\e\e[D" backward-word
        bindkey "^H" backward-delete-word
        bindkey "^?" backward-delete-char
        source ${pkgs.zsh-powerlevel9k}/share/zsh-powerlevel9k/powerlevel9k.zsh-theme
        unset __HM_SESS_VARS_SOURCED ; source ~/.nix-profile/etc/profile.d/hm-session-vars.sh
        ZSH_THEME="powerlevel9k"
        fpath=(~/.zsh/completions $fpath)
        autoload -U compinit && compinit
      '';
      oh-my-zsh = {
        enable = true;
      };
    };
  };
}
